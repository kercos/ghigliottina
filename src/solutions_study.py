import os

work_dir = '/Users/fedja/scratch/Ghigliottina/model_06_evalita_split/solutions_study'
diff_file = os.path.join(work_dir, 'diff_ghigliottiniamo_20210519.txt')
agg_freq_lex_file = '/Users/fedja/scratch/CORPORA/PAISA/lex_freq/aggettivi_freq.txt'
noun_freq_lex_file = '/Users/fedja/scratch/CORPORA/PAISA/lex_freq/sostantivi_freq.txt'
out_file = os.path.join(work_dir, 'diff_ghigliottiniamo_20210519_sorted.txt')

lex_freq = {}
with open(agg_freq_lex_file) as f_in:
    lines = [l.strip().split('\t') for l in f_in.readlines() if l.strip()]
    for l in lines:
        lex_freq[l[1]] = int(l[0])
with open(noun_freq_lex_file) as f_in:
    lines = [l.strip().split('\t') for l in f_in.readlines() if l.strip()]
    for l in lines:
        if l[1] in lex_freq:
            lex_freq[l[1]] = max(lex_freq[l[1]], int(l[0]))
        else:
            lex_freq[l[1]] = int(l[0])

print(f'read {len(lex_freq)} agg_lex_freq')

with open(diff_file) as f_in:
    solutions = [l.strip() for l in f_in.readlines() if l.strip()]
    print(f'read {len(solutions)} solutions (from diff)')

solutions_freq = {s:lex_freq[s] for s in solutions if s in lex_freq}
solutions_freq.update(
    {s:1 for s in solutions if s not in lex_freq}
)
print(f'Filtered {len(solutions_freq)} solutions')

with open(out_file, 'w') as f_out:
    for s,f in sorted(solutions_freq.items(), key=lambda x: -x[1]):
        f_out.write(f'{s}\n')
        # f_out.write(f'{s}\t{f}\n')


